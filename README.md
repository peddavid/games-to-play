# Games to play

* [ ] Life is Strange Before the Storm
* [x] Batman The Enemy Within
* [ ] Red Dead Redemption 2
* [ ] Resident Evil 1
* [ ] Resident Evil 0
* [x] Little Nightmares
* [ ] Shadow Tactics Blades of the Shogun
* [ ] Wolfenstein The New Order
* [ ] Metro
* [x] Prey
* [x] Resident Evil 2
* [x] Devil May Cry 5
* [ ] Titanfall 2

## Replays

* [ ] Dying Light
* [ ] The Stanley Parable
* [ ] Bioshock Infinite
* [ ] Devil May Cry 4

## Not yet released 

* [ ] Doom 2
* [ ] Ghost of Tsushima
